#pragma once
#include "data.hpp"
#include <sstream>
#include <ctime>
/*
    服务端业务处理类：
        文件上传
        文件展示
        文件下载
*/
extern qdycloud::DataManager *_data;
namespace qdycloud
{
    class Service
    {
    private:
        int _server_port;
        std::string _server_ip;
        std::string _download_prefix;
        httplib::Server _server;

    public:
        Service()
        {
            Config *con = Config::GetInstance();
            _server_port = con->GetServerPort();
            _server_ip = con->GetServerIp();
            _download_prefix = con->GetDownloadPrefix();
        }

        bool RunModule()
        {
            _server.Post("/upload", Upload);
            _server.Get("/listshow", ListShow);
            _server.Get("/home/qidunyan/wandu-cloud-disk/src-server/icon/icon.png", Icon);
            _server.Get("/", ListShow);
            std::string download_url = _download_prefix + "(.*)"; //(.*)正则表达式：匹配任意字符零次或多次
            _server.Get(download_url, DownLoad);
            _server.listen("0.0.0.0", _server_port);
            return true;
        }

    private:
        static void Upload(const httplib::Request &req, httplib::Response &rsp)
        {
            // 判断有没有上传的文件区域
            auto ret = req.has_file("file");
            if (ret == false)
            {
                rsp.status = 400;
                return;
            }
            const auto &file = req.get_file_value("file");
            // std::string clientip = req.remote_addr;
            // int clientport = req.remote_port;
            // std::string dir_name = clientip + "-" + std::to_string(clientport);

            // // 文件名：file.filename   文件数据：file.content
            std::string back_dir = Config::GetInstance()->GetBackDir();
            // //./backdir/xxx/a.txt
            std::string realpath = back_dir + FileUtil(file.filename).FileName();
            // // 目录不存在则先创建目录
            // if (!FileUtil(back_dir + dir_name).Exists())
            // {
            //     FileUtil(back_dir + dir_name).CreateDirectory();
            // }

            FileUtil fu(realpath);
            fu.SetContent(file.content); // 将数据写入文件

            BackupInfo info;
            info.NewBackupInfo(realpath);
            _data->Insert(info); // 向数据管理模块添加备份信息
        }
        static std::string TimetoStr(time_t t)
        {
            struct tm *tmp = localtime(&t);
            char buff[128];
            // year-month-day 时：分：秒
            snprintf(buff, sizeof(buff), "%d-%d-%d %d:%d:%d", tmp->tm_year + 1900, tmp->tm_mon + 1, tmp->tm_mday,
                     tmp->tm_hour, tmp->tm_min, tmp->tm_sec);
            return buff;
        }
        // 图标信息请求
        static void Icon(const httplib::Request &req, httplib::Response &rsp)
        {
            // 将本地文件数据读取，构造响应即可
            FileUtil fu("/home/qidunyan/wandu-cloud-disk/src-server/icon/icon.png");
            std::string body;
            fu.GetContent(&body);
            rsp.body = body;
            rsp.status = 200;
            return;
        }
        // 页面展示请求
        static void ListShow(const httplib::Request &req, httplib::Response &rsp)
        {
            // 获取所有文件备份信息
            std::vector<BackupInfo> arr;
            _data->GetALL(&arr);
            // 根据备份信息， 组织html数据
            std::string head;
            FileUtil fu("./show_module.html");
            fu.GetContent(&head);
            std::stringstream ss;
            ss << head;
            for (auto &a : arr)
            {
                // 特殊bug针对性处理
                if (a._real_path == "")
                {
                    continue;
                }
                std::string filename = FileUtil(a._real_path).FileName();
                ss << "<tr>";
                ss << R"(<td><img src='/home/qidunyan/wandu-cloud-disk/src-server/icon/icon.png' alt="文件图标" class='file-icon'><a href=')";
                ss << a._url << "'>" << filename << "</a></td>";
                ss << "<td align='right'>" << TimetoStr(a._mtime) << "</td>";
                ss << "<td align='right'>" << a._fsize / 1024 << "KB</td>";
                ss << "<td align='right'>" << a._download_count << "</td>";
                ss << "</tr>";
            }
            ss << "</table></body></html>";
            rsp.body = ss.str();
            rsp.set_header("Content-Type", "text/html");
            rsp.status = 200;
            return;
        }
        // ETag:http中的头部字段，存储了一个资源的唯一标识，可以用来判断文件是否被修改
        static std::string GetETag(const BackupInfo &info)
        {
            FileUtil fu(info._real_path);
            // 这里以 filename-fsize-mtime来组织  （http不关心etag的具体内容）
            std::string etag = fu.FileName();
            etag += "-";
            etag += std::to_string(info._fsize);
            etag += "-";
            etag += std::to_string(info._mtime);

            return etag;
        }
        // 下载请求---支持断点续传功能
        static void DownLoad(const httplib::Request &req, httplib::Response &rsp)
        {
            // 客户端请求资源路径：req.path
            // 根据资源路径，获取文件备份信息
            BackupInfo info;
            _data->GetOneByURL(req.path, &info);
            std::string clientip = req.remote_addr;
            int clientport = req.remote_port;

            // 判断文件是否被压缩，如果被压缩则进行解压缩
            if (info._pack_flag == true)
            {
                FileUtil fu(info._pack_path);
                // 将文件解压到备份目录下

                fu.UnCompress(info._real_path);
                // 删除压缩包，修改备份信息
                fu.Remove();
                info._pack_flag = false;
                _data->Update(info);
            }

            // 读取文件数据
            FileUtil fu(info._real_path);

            bool retrans = false; // 是否进行断点续传
            std::string old_etag;
            // 判断是否有If-Range字段
            if (req.has_header("If-Range"))
            {
                // 判断if-range字段中的etag是否与新的etag一致，如果不一致，说明服务端对原来下载的文件做了修改
                // 此时返回所有数据，作为新的文件下载，如果一致，则响应Range中设置的范围数据（断点续传）
                old_etag = req.get_header_value("If-Range");
                if (old_etag == GetETag(info))
                {
                    retrans = true;
                }
            }
            // 正常下载
            if (retrans == false)
            {

                fu.GetContent(&rsp.body);
                // 设置响应头部字段
                rsp.set_header("Accept-Ranges", "bytes");
                rsp.set_header("ETag", GetETag(info));
                rsp.set_header("Content-Type", "application/octet-stream"); // 文件下载
                rsp.status = 200;
            }
            else
            {

                // 断点续传
                // httblib中内部实现了断点续传，内部会对请求进行解析，判断是否有range字段
                // 如果有的话 状态码status会被设置为206，然后会将body的整体数据从范围内进行截取出range设置的范围的数据
                fu.GetContent(&rsp.body);
                rsp.set_header("Accept-Ranges", "bytes");
                rsp.set_header("Content-Type", "application/octet-stream");
                rsp.set_header("ETag", GetETag(info));
                rsp.status = 206; // httplib库内其实已经进行了设置  Content-Range字段也进行了设置
            }
            _data->AddDownloadByInfo(info); // 统计下载次数++,线程安全
            std::cout << clientip << "-" << clientport << " download a file:" << FileUtil(info._real_path).FileName() << std::endl;
        }
    };

}