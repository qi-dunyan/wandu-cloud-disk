#pragma once
#include <iostream>
#include <vector>
#include <mutex>
#include <thread>
#include <condition_variable>
#include <functional>
#include <queue>
#include "util.hpp"
#include "config.hpp"

const static int num = 5;

class ThreadPool
{
public:
    static ThreadPool *GetInstance()
    {
        // 双检查判断
        if (_instance == nullptr)
        {
            std::unique_lock<std::mutex> lock(_insmtx);
            if (_instance == nullptr)
            {
                _instance = new ThreadPool();
                return _instance;
            }
        }
    }
    void threadRoutine()
    {
        while (true)
        {
            std::string filename;
            // 该队列对于所有线程而言为共享资源，加锁保护
            {
                std::unique_lock<std::mutex> lock(_mtx);
                // 检测任务队列
                while (isEmpty())
                {
                    // 当没有任务时，线程进入阻塞
                    _cv.wait(lock);
                }
                // 获取文件名
                filename = pop();
            }
            // 并发进行任务处理
            qdycloud::FileUtil fu(filename);
            // 组织压缩路径
            qdycloud::Config *con = qdycloud::Config::GetInstance();
            std::string pack_path = con->GetPackDir() + fu.FileName() + con->GetPackfileSuffix();
            // 压缩
            std::cout << std::this_thread::get_id() << "线程正在压缩文件->" << filename << std::endl;
            fu.Compress(pack_path);
            std::cout << std::this_thread::get_id() << "线程压缩完毕->" << pack_path << std::endl;

            // 删除原文件
            fu.Remove();
            // std::cout << std::this_thread::get_id() << "线程删除原文件->" << filename << std::endl;
        }
    }

    // void Print()
    // {
    //     std::queue<std::string> tmp(_tq);
    //     std::cout << "------文件名队列------\n";
    //     while (!tmp.empty())
    //     {
    //         std::cout << tmp.front() << std::endl;
    //         tmp.pop();
    //     }
    // }

    std::string pop()
    {
        std::string tmp = _tq.front();
        _tq.pop();
        return tmp;
    }
    bool HasnoFile(const std::string &filename)
    {
        std::queue<std::string> tmp(_tq);
        while (!tmp.empty())
        {
            if (filename == tmp.front())
            {
                return false;
            }
            tmp.pop();
        }
        return true;
    }
    void push(const std::string &filename)
    {

        std::unique_lock<std::mutex> lock(_mtx);
        if (HasnoFile(filename) == false)
        {
            return;
        }
        std::cout << "To ThreadPool push a filename :" << filename << std::endl;
        _tq.push(filename);
        // 唤醒线程进行处理
        _cv.notify_one();
        return;
    }

    static void DelInstance()
    {
        std::unique_lock<std::mutex> lock(_delmtx);
        if (_instance)
        {
            delete _instance;
            _instance = nullptr;
        }
    }
    // 单例对象的内部回收机制
    class GC
    {
    public:
        ~GC()
        {
            DelInstance();
        }
    };
    GC _gc; // 程序结束时自动调用析构

    ~ThreadPool()
    {
        for (auto &ch : _threads)
        {
            ch.join();
        }
    }

private:
    bool isEmpty()
    {
        return _tq.empty();
    }

    ThreadPool(int tnum = num)
        : _tnum(tnum), _threads(_tnum)
    {
        for (int i = 0; i < _tnum; ++i)
        {
            _threads[i] = std::thread(std::bind(&ThreadPool::threadRoutine, this));
        }
    }

    int _tnum;
    std::vector<std::thread> _threads;
    std::mutex _mtx;             // 互斥锁
    std::condition_variable _cv; // 条件变量

    std::queue<std::string> _tq;
    // // 任务队列---这里用来存放文件名信息
    static ThreadPool *_instance;
    static std::mutex _insmtx;
    static std::mutex _delmtx;
};
// 类外初始化
ThreadPool *ThreadPool::_instance = nullptr;
std::mutex ThreadPool::_insmtx;
std::mutex ThreadPool::_delmtx;