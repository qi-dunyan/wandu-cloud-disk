#pragma once
#include <unordered_map>
#include <pthread.h>
#include "util.hpp"
#include "config.hpp"
#include "md5file.h"

namespace qdycloud
{
    /*数据信息类*/
    struct BackupInfo
    {
        bool _pack_flag;        // 标志位--是否为压缩文件
        size_t _fsize;          // 文件大小
        time_t _mtime;          // 最后一次修改时间
        time_t _atime;          // 最后一次访问时间
        std::string _real_path; // 实际路径
        std::string _pack_path; // 压缩包所在路径
        std::string _url;       // 文件访问url中的path

        int _download_count; // 记录下载次数
        std::string _md5sum;

        // std::string _clientip;
        // std::string _clientport;

        void NewBackupInfo(const std::string &realpath)
        {
            Config *con = Config::GetInstance();
            std::string packdir = con->GetPackDir();
            std::string packsuffix = con->GetPackfileSuffix();
            std::string download_prefix = con->GetDownloadPrefix();
            // ./backdir/a.txt  -->  ./packdir/a.txt/lz
            FileUtil fu(realpath);
            _pack_flag = false;
            _fsize = fu.FileSize();
            _mtime = fu.LastMTime();
            _atime = fu.LastATime();
            _real_path = realpath;
            _pack_path = packdir + fu.FileName() + packsuffix;
            _download_count = con->GetDownloadCount();
            _md5sum = getFileMD5(realpath);
            //./backdir/a.txt  ->/download/a.txt
            _url = download_prefix + fu.FileName();
        }
    };

    /*数据信息管理类*/
    class DataManager
    {
    private:
        pthread_rwlock_t _rwlock; // 读写锁，读共享，写互斥
        std::string _backup_file;
        std::unordered_map<std::string, BackupInfo> _table;

    public:
        DataManager()
        {
            _backup_file = Config::GetInstance()->GetBackupFile(); // 备份文件路径
            // 初始化读写锁
            pthread_rwlock_init(&_rwlock, NULL);
            InitLoad();
        }
        ~DataManager()
        {
            pthread_rwlock_destroy(&_rwlock);
        }

        bool AddDownloadByInfo(BackupInfo &info)
        {
            pthread_rwlock_wrlock(&_rwlock);
            ++info._download_count;
            pthread_rwlock_unlock(&_rwlock);
            Update(info);

            return true;
        }

        bool Insert(const BackupInfo &info)
        {
            // 加写锁
            pthread_rwlock_wrlock(&_rwlock);
            _table[info._url] = info;
            pthread_rwlock_unlock(&_rwlock);
            Storage();
            return true;
        }
        bool Update(const BackupInfo &info)
        {
            pthread_rwlock_wrlock(&_rwlock);
            _table[info._url] = info;
            pthread_rwlock_unlock(&_rwlock);
            Storage();

            return true;
        }

        bool GetOneByURL(const std::string &url, BackupInfo *info)
        {
            pthread_rwlock_wrlock(&_rwlock);
            // 直接通过key来查找
            auto it = _table.find(url);
            if (it == _table.end())
            {
                pthread_rwlock_unlock(&_rwlock);
                return false;
            }
            *info = it->second;
            pthread_rwlock_unlock(&_rwlock);
            return true;
        }
        bool GetOneByRealpath(const std::string &realpath, BackupInfo *info)
        {
            pthread_rwlock_wrlock(&_rwlock);
            auto it = _table.begin();
            for (; it != _table.end(); ++it)
            {
                if (it->second._real_path == realpath)
                {
                    *info = it->second;
                    pthread_rwlock_unlock(&_rwlock);
                    return true;
                }
            }
            pthread_rwlock_unlock(&_rwlock);
            return false;
        }
        bool GetALL(std::vector<BackupInfo> *array)
        {

            pthread_rwlock_wrlock(&_rwlock);
            auto it = _table.begin();
            for (; it != _table.end(); ++it)
            {
                array->push_back(it->second);
            }
            pthread_rwlock_unlock(&_rwlock);
            return true;
        }
        bool InitLoad()
        {
            // 读取数据备份文件中的数据
            std::cout << _backup_file << std::endl;
            FileUtil fu(_backup_file);
            if (fu.Exists() == false)
            {
                return true;
            }
            std::string body;
            fu.GetContent(&body);
            // 反序列化
            Json::Value root;
            JsonUtil::UnSerialize(body, &root);
            // 将反序列化得到的Json::Value各项数据添加
            for (int i = 0; i < root.size(); ++i)
            {
                BackupInfo info;
                info._atime = root[i]["atime"].asInt64();
                info._fsize = root[i]["fsize"].asInt64();
                info._mtime = root[i]["mtime"].asInt64();
                info._pack_flag = root[i]["pack_flag"].asBool();
                info._download_count = root[i]["download_count"].asInt();
                info._pack_path = root[i]["pack_path"].asString();
                info._real_path = root[i]["real_path"].asString();
                info._url = root[i]["url"].asString();
                info._md5sum = root[i]["md5sum"].asString();
                Insert(info);
            }
            return true;
        }
        // 持久化存储数据
        bool Storage()
        {
            // 先获取所有数据
            std::vector<BackupInfo> array;
            GetALL(&array);
            // 将数据填充到Json::value中
            Json::Value root;
            for (int i = 0; i < array.size(); ++i)
            {
                Json::Value item;
                item["pack_flag"] = array[i]._pack_flag;
                item["fsize"] = (Json::Int64)array[i]._fsize;
                item["download_count"] = array[i]._download_count;
                item["mtime"] = (Json::Int64)array[i]._mtime;
                item["atime"] = (Json::Int64)array[i]._atime;
                item["real_path"] = array[i]._real_path;
                item["pack_path"] = array[i]._pack_path;
                item["url"] = array[i]._url;
                item["md5sum"] = array[i]._md5sum;
                root.append(item);
            }
            // 序列化
            std::string body;
            JsonUtil::Serialize(root, &body);
            // 将数据写入文件
            FileUtil fu(_backup_file);
            fu.SetContent(body);
            return true;
        }
    };

}
