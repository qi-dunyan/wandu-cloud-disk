#pragma once
#include <mutex>
#include <cassert>
#include "util.hpp"

/*配置文件类的设计*/

#define CONFIG_FILE "./cloud.conf"
namespace qdycloud
{
    class Config
    {
    private:
        static std::mutex _mutex;
        static Config *_instance;

    private:
        int _hot_time;                // 热点判断时间
        int _server_port;             // 服务器监听接口
        int _download_count;          // 文件下载次数
        std::string _server_ip;       // 服务器ip地址
        std::string _download_prefix; // 下载的url前缀路径
        std::string _packfile_suffix; // 压缩包后缀
        std::string _pack_dir;        // 压缩包存放目录
        std::string _back_dir;        // 备份文件存放目录
        std::string _backup_file;     // 数据信息存放文件

    public:
        static Config *GetInstance()
        {
            if (_instance == nullptr)
            {
                _mutex.lock();
                if (_instance == nullptr)
                {
                    _instance = new Config();
                }
                _mutex.unlock();
            }
            return _instance;
        }

        int GetHotTime() { return _hot_time; }
        int GetServerPort() { return _server_port; }
        int GetDownloadCount() { return _download_count; }
        std::string GetServerIp() { return _server_ip; }
        std::string GetDownloadPrefix() { return _download_prefix; }
        std::string GetPackfileSuffix() { return _packfile_suffix; }
        std::string GetPackDir() { return _pack_dir; }
        std::string GetBackDir() { return _back_dir; }
        std::string GetBackupFile() { return _backup_file; }
        // 显示的释放单例对象
        static void DelInstance()
        {
            _mutex.lock();
            if (_instance)
            {
                delete _instance;
                _instance = nullptr;
            }
            _mutex.unlock();
        }

        // 单例对象的内部回收机制
        class GC
        {
        public:
            ~GC()
            {
                DelInstance();
            }
        };
        GC _gc; // 程序结束时自动调用析构

    private:
        Config()
        {
            assert(ReadConfigFile());
        }
        Config(const Config &con) = delete;
        bool ReadConfigFile()
        {
            FileUtil fu(CONFIG_FILE);
            std::string body;
            if (false == fu.GetContent(&body))
            {
                std::cout << "get configfile failed\n";
                return false;
            }
            Json::Value root;
            if (false == JsonUtil::UnSerialize(body, &root))
            {
                std::cout << "parse configfile failed\n";
                return false;
            }
            _hot_time = root["hot_time"].asInt();
            _server_port = root["server_port"].asInt();
            _server_ip = root["server_ip"].asString();
            _download_prefix = root["download_prefix"].asString();
            _packfile_suffix = root["packfile_suffix"].asString();
            _pack_dir = root["pack_dir"].asString();
            _back_dir = root["back_dir"].asString();
            _backup_file = root["backup_file"].asString();
            _download_count = root["download_count"].asInt();

            return true;
        }
    };
    Config *Config::_instance = nullptr;
    std::mutex Config::_mutex;

}
