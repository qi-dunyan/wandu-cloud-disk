#pragma once
#include <ctime>
#include <unistd.h>
#include "data.hpp"
#include "threadpool.hpp"
#include "md5file.h"
extern qdycloud::DataManager *_data;
namespace qdycloud
{
    /*热点管理类*/
    class HotManager
    {
    private:
        std::string _pack_dir;
        std::string _back_dir;
        std::string _pack_suffix;
        int _hot_time;

    private:
        // 非热点文件返回真，否则返回假
        bool HotJudge(const std::string &filename)
        {
            FileUtil fu(filename);
            time_t last_atime = fu.LastATime();
            time_t cur_time = time(nullptr);
            if (cur_time - last_atime > _hot_time)
            {
                return true;
            }
            return false;
        }

    public:
        HotManager()
        {
            Config *con = Config::GetInstance();
            _pack_dir = con->GetPackDir();
            _back_dir = con->GetBackDir();
            _pack_suffix = con->GetPackfileSuffix();
            _hot_time = con->GetHotTime();

            FileUtil fu1(_pack_dir);
            FileUtil fu2(_back_dir);
            if (fu1.Exists() == false)
            {
                fu1.CreateDirectory();
            }
            if (fu2.Exists() == false)
            {
                fu2.CreateDirectory();
            }
        }
        bool RunModule()
        {
            while (true)
            {
                // 遍历备份目录，获取所有文件名
                FileUtil fu(_back_dir);
                std::vector<std::string> arr;
                fu.ScanDirectory(&arr);
                // 遍历判断文件是否非热点文件--对非热点文件进行压缩--删除源文件，修改备份信息
                for (auto &a : arr)
                {
                    // 热点文件不需要进行处理
                    if (HotJudge(a) == false)
                    {
                        continue;
                    }
                    // 获取文件备份信息
                    BackupInfo bi;
                    if (_data->GetOneByRealpath(a, &bi) == false)
                    {
                        // 文件存在，但是文件没有备份信息--创建备份信息
                        bi.NewBackupInfo(a);
                    }
                    // 获取当前的MD5
                    std::string md5 = getFileMD5(a);

                    // 非热点文件如果没有被压缩，则push进线程池，唤醒线程来进行压缩
                    if (bi._pack_flag == false)
                    {
                        // 线程池，将文件名放入线程池，唤醒线程来进行压缩
                        ThreadPool::GetInstance()->push(a);
                        // 修改备份信息
                        bi._pack_flag = true;
                        _data->Update(bi);
                    }
                    else
                    {
                        // 压缩路径下存在同名压缩包，判断是否需要对该文件进行再次压缩
                        /*
                            这里存在一个小bug，就是对同一个文件，只会保留第一次上传的那个文件，后续如果
                            对文件的内容做了修改，这里的策略统一直接remove

                            ******
                            新增功能，对同名文件的md5值进行判断---已解决上述bug
                        */

                        // 此时说明，存在压缩后的同名文件---计算MD5值进行比较
                        if (FileUtil(bi._real_path).Exists() == true)
                        {
                            std::string backmd5 = bi._md5sum;
                            if (md5 == backmd5)
                            {
                                std::cout << "文件已存在\n";
                                FileUtil(a).Remove(); // 删除当前文件
                            }
                            else
                            {
                                bi._pack_flag = false;
                                bi._md5sum = md5;
                                std::cout << "文件发生修改\n";
                                _data->Update(bi);
                            }
                        }
                    }
                }
                // ThreadPool::GetInstance()->Print();
                usleep(1000); // 避免空目录，消耗cpu资源过多
            }
            return true;
        }
    };

}