#include "util.hpp"
#include "config.hpp"
#include "data.hpp"
#include "hot.hpp"
#include "service.hpp"
#include <thread>

void test_util_1(const std::string &filename)
{
    qdycloud::FileUtil fu(filename);
    std::cout << fu.FileName() << std::endl;
    std::cout << fu.FileSize() << std::endl;
    std::cout << fu.LastMTime() << std::endl;
    std::cout << fu.LastATime() << std::endl;
}

void test_util_2(const std::string &filename)
{
    qdycloud::FileUtil fu(filename);
    std::string body;

    // 读取一个文件，将数据写入另一个文件，进行判断
    fu.GetContent(&body);

    qdycloud::FileUtil fu2("./hello.txt");
    fu2.SetContent(body);

    return;
}

void test_util_3(const std::string &filename)
{
    // 压缩包名称
    qdycloud::FileUtil fu(filename); // 将数据进行压缩
    std::string packname = filename + ".lz";
    fu.Compress(packname);

    // 解压到另一个文件
    qdycloud::FileUtil nfu(packname);
    nfu.UnCompress("./uncom.txt");
}

void test_util_4(const std::string &filename)
{
    qdycloud::FileUtil fu(filename);
    fu.CreateDirectory();
    std::vector<std::string> arr;
    fu.ScanDirectory(&arr);
    for (auto &a : arr)
    {
        std::cout << a << std::endl;
    }
}

void json_test()
{
    const char *name = "张三 ";
    int age = 18;
    double source[] = {88.8, 89, 94.5};
    // 将数据保存在json：value对象中
    Json::Value root;
    root["姓名"] = name;
    root["年龄"] = age;
    for (int i = 0; i < 3; ++i)
    {
        root["成绩"].append(source[i]);
    }
    std::string str;
    qdycloud::JsonUtil::Serialize(root, &str);
    std::cout << str << std::endl;

    Json::Value uroot;
    qdycloud::JsonUtil::UnSerialize(str, &uroot);

    std::cout << uroot["姓名"].asString() << std::endl;
    std::cout << uroot["年龄"].asInt() << std::endl;
    for (int i = 0; i < 3; ++i)
    {
        std::cout << uroot["成绩"][i].asDouble() << " ";
    }
    std::cout << std::endl;
}

void config_test()
{
    qdycloud::Config *con = qdycloud::Config::GetInstance();
    std::cout << con->GetBackDir() << std::endl;
    std::cout << con->GetBackupFile() << std::endl;
    std::cout << con->GetDownloadCount() << std::endl;
    std::cout << con->GetDownloadPrefix() << std::endl;
    std::cout << con->GetHotTime() << std::endl;
    std::cout << con->GetPackDir() << std::endl;
    std::cout << con->GetPackfileSuffix() << std::endl;
    std::cout << con->GetServerIp() << std::endl;
    std::cout << con->GetServerPort() << std::endl;
}

void data_test(const std::string &filename)
{
    qdycloud::BackupInfo info;
    info.NewBackupInfo(filename);
    std::cout << info._atime << std::endl;
    std::cout << info._download_count << std::endl;
    std::cout << info._fsize << std::endl;
    std::cout << info._mtime << std::endl;
    std::cout << info._pack_flag << std::endl;
    std::cout << info._pack_path << std::endl;
    std::cout << info._real_path << std::endl;
    std::cout << info._url << std::endl;
}

void data_test_manager(const std::string &filename)
{
    qdycloud::DataManager data;
    std::vector<qdycloud::BackupInfo> arr;
    data.GetALL(&arr);
    for (auto &a : arr)
    {
        std::cout << a._atime << std::endl;
        std::cout << a._download_count << std::endl;
        std::cout << a._fsize << std::endl;
        std::cout << a._mtime << std::endl;
        std::cout << a._pack_flag << std::endl;
        std::cout << a._pack_path << std::endl;
        std::cout << a._real_path << std::endl;
        std::cout << a._url << std::endl;
    }

    // qdycloud::BackupInfo info;
    // info.NewBackupInfo(filename);
    // qdycloud::DataManager data;
    // data.Insert(info);

    // qdycloud::BackupInfo tmp;
    // data.GetOneByURL("/download/bundle.h", &tmp);
    // std::cout << "--------GetOneByURL & Insert--------------\n";
    // std::cout << tmp._atime << std::endl;
    // std::cout << tmp._download_count << std::endl;
    // std::cout << tmp._fsize << std::endl;
    // std::cout << tmp._mtime << std::endl;
    // std::cout << tmp._pack_flag << std::endl;
    // std::cout << tmp._pack_path << std::endl;
    // std::cout << tmp._real_path << std::endl;
    // std::cout << tmp._url << std::endl;

    // info._pack_flag = true;
    // data.Update(info);
    // std::vector<qdycloud::BackupInfo> arr;
    // std::cout << "--------GetALL & Update--------------\n";
    // data.GetALL(&arr);
    // for (auto &a : arr)
    // {
    //     std::cout << a._atime << std::endl;
    //     std::cout << a._download_count << std::endl;
    //     std::cout << a._fsize << std::endl;
    //     std::cout << a._mtime << std::endl;
    //     std::cout << a._pack_flag << std::endl;
    //     std::cout << a._pack_path << std::endl;
    //     std::cout << a._real_path << std::endl;
    //     std::cout << a._url << std::endl;
    // }
    // std::cout << "----------------GetOneByRealpath-----------------\n";
    // data.GetOneByRealpath(filename, &tmp);
    // std::cout << tmp._atime << std::endl;
    // std::cout << tmp._download_count << std::endl;
    // std::cout << tmp._fsize << std::endl;
    // std::cout << tmp._mtime << std::endl;
    // std::cout << tmp._pack_flag << std::endl;
    // std::cout << tmp._pack_path << std::endl;
    // std::cout << tmp._real_path << std::endl;
    // std::cout << tmp._url << std::endl;
    // data.Storage();
}

qdycloud::DataManager *_data;
void hot_test()
{
    qdycloud::HotManager hot;
    hot.RunModule();
}

void service_test()
{
    qdycloud::Service svr;
    svr.RunModule();
}

int main(int argc, char *argv[])
{
    _data = new qdycloud::DataManager();
    std::thread hot_t(hot_test);
    std::thread service_t(service_test);
    hot_t.join();
    service_t.join();
    return 0;

    /*以下为各个模块测试*/
    /////////////////////////////////////////////////////
    // std::string filename = argv[1];
    // test_util_1(filename);
    // test_util_2(filename);
    // test_util_3(filename);
    // test_util_4(filename);
    // json_test();
    // config_test();
    // data_test(filename);
    // data_test_manager(filename);
    return 0;
}