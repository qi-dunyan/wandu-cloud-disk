#pragma once
/*
    实用工具类的设计：
        1、获取文件大小
        2、最后修改时间
        3、最后访问时间
        4、获取文件路径名中的文件名称
        5、向文件写入数据
        6、读取文件数据
        7、获取文件指定位置的指定长度的数据--断点续传
        8、获取目录中的所有文件信息
        9、判断文件（目录）是否存在
        10、创建目录
*/
#include <iostream>
#include <string>
#include <sys/stat.h>
#include <fstream>
#include <vector>
#include <memory>
#include<experimental/filesystem>

namespace qdycloud
{
    namespace fs = std::experimental::filesystem;
    /*文件实用工具类*/
    class FileUtil
    {
    private:
        std::string _filename;

    public:
        FileUtil(const std::string &filename) : _filename(filename)
        {
        }
        // 文件大小
        size_t FileSize()
        {
            struct stat st;
            if (stat(_filename.c_str(), &st) < 0)
            {
                std::cout << "get file size failed\n";
                return 0;
            }
            return st.st_size;
        }
        // 最后访问时间
        time_t LastATime()
        {
            struct stat st;
            if (stat(_filename.c_str(), &st) < 0)
            {
                std::cout << "get file atime failed\n";
                return -1;
            }
            return st.st_atime;
        }
        // 最后修改时间
        time_t LastMTime()
        {
            struct stat st;
            if (stat(_filename.c_str(), &st) < 0)
            {
                std::cout << "get file mtime failed\n";
                return -1;
            }
            return st.st_mtime;
        }
        // 获取文件名称
        std::string FileName()
        {
            //./a/b.txt
            auto pos = _filename.find_last_of("\\");
            if (pos == std::string::npos)
            {
                return _filename;
            }
            return _filename.substr(pos + 1);
        }
        // 读取文件数据 & 从指定位置读取指定长度数据
        bool GetPosLen(std::string *body, size_t pos, size_t len)
        {
            size_t fsize = FileSize();
            if (pos + len > fsize)
            {
                std::cout << "get file len is error\n";
                return false;
            }
            std::ifstream ifs;
            // 打开文件
            ifs.open(_filename, std::ios::binary);
            if (ifs.is_open() == false)
            {
                std::cout << "GetPosLen:open file failed\n";
                return false;
            }

            // 偏移指针
            ifs.seekg(pos, std::ios::beg);
            body->resize(fsize);
            // 读取数据
            ifs.read(&(*body)[0], len);
            if (ifs.good() == false)
            {
                std::cout << "get file Content failed\n";
                ifs.close();
                return false;
            }

            ifs.close();
            return true;
        }
        bool GetContent(std::string *body)
        {
            size_t fsize = FileSize();
            return GetPosLen(body, 0, fsize);
        }
        // 写入数据
        bool SetContent(const std::string &body)
        {
    
            // 打开文件
            std::ofstream ofs;
            ofs.open(_filename, std::ios::binary);
            if (ofs.is_open() == false)
            {
                std::cout << "write open file failed\n";
                return false;
            }
            // write
            ofs.write(&body[0], body.size());
            if (ofs.good() == false)
            {
                std::cout << "write file content failed\n";
                ofs.close();
                return false;
            }
            ofs.close();
            return true;
        }
        // 文件是否存在
        bool Exists()
        {
            return fs::exists(_filename);
        }
        // 创建目录
        bool CreateDirectory()
        {
            if (Exists())
            {
                return true;
            }
            return fs::create_directories(_filename);
        }
        // 获取目录下的所有文件名称
        bool ScanDirectory(std::vector<std::string> *array)
        {
            CreateDirectory();
            for (auto &p : fs::directory_iterator(_filename))
            {
                // 如果是一个目录，直接跳过
                if (fs::is_directory(p) == true)
                {
                    continue;
                }
                // relative_path()带有路径的文件名
                array->push_back(fs::path(p).relative_path().string());
            }

            return true;
        }
        // 删除文件
        bool Remove()
        {
            if (Exists() == false)
            {
                return true;
            }
            remove(_filename.c_str());
            return true;
        }
    };
   

}
