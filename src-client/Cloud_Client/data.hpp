#pragma once
#include<unordered_map>
#include<sstream>
#include<functional>
#include<mutex>
#include"util.hpp"
//客户端功能：对指定文件夹内的文件进行自动备份上传
/*
    数据信息管理类的设计：
	    将备份文件的信息管理起来，用于判断下一次文件是否需要进行备份（数据如果发生修改，则进行自动上传备份）
*/
namespace qdycloud
{
	class DataManager
	{
	private:
		std::string _backup_file;//备份信息的持久化存储文件
								 //key:filename val:文件唯一标识-判断文件是否需要备份上传
		std::unordered_map<std::string, std::string> _table;

	public:
		DataManager(const std::string& backup_file)
			:_backup_file(backup_file)
		{
			InitLoad();
		}
		//数据持久化存储
		bool Storage()
		{
			//获取备份信息
			std::stringstream ss;
			auto it = _table.begin();
			for (; it != _table.end(); ++it)
			{
				//将信息组织成存储格式
				//./a.txt a.txt-fsize-mtime\n
				ss << it->first << " " << it->second << "\n";
			}
			//将信息输出到指定文件
			FileUtil fu(_backup_file);
			fu.SetContent(ss.str());
			
			return true;
		}
		//分割函数
		int Split(const std::string& str, const std::string& sep, std::vector<std::string>* arr)
		{
			int count = 0;
			size_t pos, idx = 0;
			while (1)
			{
				pos = str.find(sep, idx);
				if (pos == std::string::npos) { break; }
				//连续空格
				if (pos == idx)
				{
					idx = pos + sep.size();
					continue;
				}
				std::string tmp = str.substr(idx, pos - idx);
				arr->push_back(tmp);
				++count;
				idx = pos + sep.size();
			}
			//最后一个数据
			if (idx < str.size())
			{
				arr->push_back(str.substr(idx));
				++count;
			}
			return count;
		}
		bool InitLoad()
		{
			//读取文件数据 
			FileUtil fu(_backup_file);
			std::string body;
			fu.GetContent(&body);
			//先按行读取数据
			std::vector<std::string> arr;
			Split(body, "\n", &arr);
			//再按空格进行读取
			for (auto& a : arr)
			{
				std::vector<std::string> tmp;
				Split(a, " ", &tmp);
				if (tmp.size() != 2) { continue; }
				_table[tmp[0]] = tmp[1];
			}
			return true;
		}

		bool Insert(const std::string& key, const std::string& val)
		{
			_table[key] = val;
			Storage();
			return true;
		}
		bool Update(const std::string& key, const std::string& val)
		{
			_table[key] = val;
			Storage();
			return true;
		}
		bool GetOneByKey(const std::string& key, std::string* val)
		{
			auto it = _table.find(key);
			if (it == _table.end())
			{
				return false;
			}
			*val = it->second;

			return true;
		}
	};


}