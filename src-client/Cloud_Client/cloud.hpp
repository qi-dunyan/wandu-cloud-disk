#pragma once
#include"httplib.h"
#include"data.hpp"
#include"md5file.h"
#include<Windows.h>


/*
    客户端备份文件类的设计：
	    检测指定目录下的文件，进行备份
*/

namespace qdycloud
{
#define SERVER_PORT 6688
#define SERVER_ADDR "43.143.242.13"
	class Backup
	{
	private:
		std::string _back_dir;//指定目录
		DataManager* _data;
	public:
		Backup(const std::string& back_dir, const std::string& back_file)
			:_back_dir(back_dir)
		{
			if (_data == nullptr)
				_data = new DataManager(back_file);
		}
		bool RunModule()
		{
			while (true)
			{
				//遍历获取指定文件夹中的所有文件
				FileUtil fu(_back_dir);
				std::vector<std::string> arr;
				fu.ScanDirectory(&arr);
				for (auto& a : arr)
				{
					if (IsNeedUpload(a) == false)
					{
						continue;
					}
					else
					{
						Upload(a);
						_data->Insert(a, GetFileIdentifier(a));
						std::cout << "上传成功，备份文件信息\n";
					}
				}

				Sleep(10);
			}
			return true;
		}
	private:
		//获取文件唯一标识 filename-fsize-mtime  --> filename-md5
		static std::string GetFileIdentifier(const std::string& filename)
		{
			FileUtil fu(filename);
			std::stringstream ss;
			ss << fu.FileName() << "-" << getFileMD5(filename);
			return ss.str();
		}
		//判断是否需要进行上传
		bool IsNeedUpload(const std::string& filename)
		{
			//计算判断文件的MD5值
			std::string filemd5;
			if (_data->GetOneByKey(filename, &filemd5) != false)
			{
				std::string newfilemd5 = GetFileIdentifier(filename);
				if (filemd5 == newfilemd5)
				{
					return false;
				}
			}
			FileUtil fu(filename);
			//特殊情况，一个文件可能处于正在修改状态--md5会一直改变，因此进行判断
			//当前时间-最后修改时间
			if (time(NULL) - fu.LastMTime() < 10)//说明10秒内正在处于修改数据
			{
				return false;
			}

			std::cout << filename << "需要上传\n";
			return true;

			////需要上传：文件是新增的----没有备份信息   文件发生了修改---有备份信息，但唯一标识不同
			//std::string id;
			//if (_data->GetOneByKey(filename, &id) != false)
			//{
			//	std::string new_id = GetFileIdentifier(filename);
			//	if (new_id == id)
			//	{
			//		return false;
			//	}
			//}
			////特殊情况：假如一个文件很大，正在缓慢的拷贝到该目录下，则该文件的信息会在一直发生改变
			////则会导致一个很大的文件会被上传很多次，因此进行特殊判断
			//FileUtil fu(filename);
			//if (time(NULL) - fu.LastMTime() < 10) //10秒内处于修改状态，则不需要上传
			//{
			//	return false;
			//}
			//std::cout <<"---- " << filename <<" ---- " << "needto upload ----\n";
			//return true;
		}
	private:
		 bool Upload(const std::string& filename)
		{
			 std::cout <<filename<< "upload..........\n";
			//获取文件数据
			FileUtil fu(filename);
			std::string body;
			fu.GetContent(&body);
			//搭建http客户端上传数据
			httplib::Client client(SERVER_ADDR, SERVER_PORT);
			httplib::MultipartFormData item;
			item.filename = fu.FileName();
			item.content = body;
			item.name = "file";
			item.content_type = "application/octet-stream";

			httplib::MultipartFormDataItems items;
			items.push_back(item);
			auto res=client.Post("/upload",items);
			if (!res || res->status!=200)
			{
				return false;
			}
			std::cout << "!!!! " << filename << " upload success !!!!\n";
			return true;
		}
	};
}